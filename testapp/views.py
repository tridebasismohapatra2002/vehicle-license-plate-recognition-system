from django.shortcuts import render
from django.http import JsonResponse
import numpy as np
from PIL import Image
from testapp import utils
from .models import VehicleEntry
from datetime import datetime


def home(request):
    if request.method == 'POST':
        image = request.FILES.get('image')
        image = Image.open(image)
        image = np.array(image)
        multiple = utils.detect_plate(image)
        data = {'prediction' : multiple}
        store_data(multiple)
        return JsonResponse(data)
    return render(request  , 'testapp/home.html')

def store_data(multiple):
    current_date = datetime.now().date()
    current_time = datetime.now().time()
    for vehicle_number in multiple:
        if len(vehicle_number) > 0:
            VehicleEntry.objects.create(vehicle_number=vehicle_number, date=current_date, time=current_time)



