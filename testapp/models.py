from django.db import models

class VehicleEntry(models.Model):
    vehicle_number = models.CharField(max_length=20)
    date = models.DateField()
    time = models.TimeField()
