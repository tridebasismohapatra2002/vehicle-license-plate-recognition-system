from django.contrib import admin
from .models import VehicleEntry 

class VehicleEntryAdmin(admin.ModelAdmin):
    list_display = ('vehicle_number', 'date', 'time')

admin.site.register(VehicleEntry, VehicleEntryAdmin)
