import cv2
import easyocr
from paddleocr import PaddleOCR
import yolov5
import numpy as np
import re

model = yolov5.load('./custom_best.pt')

ocr1 = PaddleOCR(lang='en',rec_algorithm='CRNN') 

ocr2 = easyocr.Reader(['en'] , gpu = False)

def remove_specialchar(s):
    cleaned_text = re.sub('[^a-zA-Z0-9]' , '' , s)
    return cleaned_text

def read_number(resized_image):

    result = ocr1.ocr(resized_image, cls=False, det=False)

    text = remove_specialchar(result[0][0][0])

    if len(text) < 7:
        new_text = ocr2.readtext(resized_image)

        recognized_text = ''

        for detection in new_text:
            text = detection[1]
            recognized_text += text

        recognized_text = remove_specialchar(recognized_text)
        
        if len(recognized_text) > 10:
            L = [char for char in recognized_text]

            for i in range(len(recognized_text)-10):
                L.remove(L[0])
            refined_text = ''.join(L)

            return refined_text

        return recognized_text

    elif len(text) > 10:
        L = [char for char in text]

        for i in range(len(text)-10):
            L.remove(L[0])
        recognized_text = ''.join(L)

        return recognized_text

    else:
        return text

def detect_plate(image):

    results = model(image)

    multiple = []

    if len(results.pred[0]) == 0:
        exception_image = cv2.cvtColor(image,cv2.COLOR_RGB2GRAY)
        exception_image = cv2.GaussianBlur(exception_image, (3, 3), 0)
        kernel = np.ones((3, 3), np.uint8)
        exception_image = cv2.morphologyEx(exception_image, cv2.MORPH_CLOSE, kernel)
        text = read_number(exception_image)
        multiple.append(text)
        return multiple

    else:

        for i in results.pred[0]:

            X_min , Y_min , X_max , Y_max = np.array(i[:4])

            plate_image = image[int(Y_min):int(Y_max) , int(X_min):int(X_max)]

            plate_image = cv2.cvtColor(plate_image,cv2.COLOR_RGB2GRAY)

            resized_image = cv2.resize(plate_image , (1080 , 330))

            resized_image = cv2.GaussianBlur(resized_image, (3, 3), 0)
            kernel = np.ones((3, 3), np.uint8)
            resized_image = cv2.morphologyEx(resized_image, cv2.MORPH_CLOSE, kernel)

            text = read_number(resized_image)

            multiple.append(text)

        return multiple



